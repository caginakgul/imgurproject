package cagin.com.imgurproject.data.retrofit.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class ImageModel(val id: String?, val title: String?, val comment_count: Int?,
                 val score: Int?, val downs: Int?, val ups: Int?, val images: List<ImageGallery>) : Parcelable {

}