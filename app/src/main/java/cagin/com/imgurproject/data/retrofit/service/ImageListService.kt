package cagin.com.imgurproject.data.retrofit.service

import android.provider.SyncStateContract
import cagin.com.imgurproject.data.retrofit.model.ImageResponse
import cagin.com.imgurproject.utils.Constants
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface ImageListService {
    @Headers("Authorization: Client-ID ec79417a9abe10a","User-Agent: FlyProject")
    @GET("hot/0.json")
    fun sendImageListRequest(): Call<ImageResponse>
}