package cagin.com.imgurproject.data.retrofit.implementation

import android.util.Log
import cagin.com.imgurproject.data.retrofit.model.ImageResponse
import cagin.com.imgurproject.data.retrofit.service.ImageListService
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject

class ImageListServiceImpl @Inject constructor(builder: Retrofit.Builder) : ImageListService {
    private val imageListService: ImageListService
    private val LOG_TAG: String ="Request"
    private val LOG_INFO: String ="sendImageListRequest"

    init {
        imageListService = builder.build().create(ImageListService::class.java)
    }

    override fun sendImageListRequest(): Call<ImageResponse> {
        Log.d(LOG_TAG,LOG_INFO)
        return imageListService.sendImageListRequest()
    }
}