package cagin.com.imgurproject.data.retrofit.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class ImageGallery(val link: String) : Parcelable {
}