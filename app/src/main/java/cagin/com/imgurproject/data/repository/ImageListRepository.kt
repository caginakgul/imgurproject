package cagin.com.imgurproject.data.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import cagin.com.imgurproject.data.retrofit.implementation.ImageListServiceImpl
import cagin.com.imgurproject.data.retrofit.model.ImageResponse
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class ImageListRepository @Inject constructor(imageListImp: ImageListServiceImpl){

    val imageListImplementation : ImageListServiceImpl
    init {
        imageListImplementation = imageListImp
    }

    fun sendImageListRequest(): LiveData<ImageResponse> {
        val data2 : MutableLiveData<ImageResponse> = MutableLiveData()
        imageListImplementation.sendImageListRequest().enqueue((object : retrofit2.Callback<ImageResponse> {
            override fun onFailure(call: Call<ImageResponse>?, t: Throwable?) {
                Log.d("request","fail")
                data2.value = null
            }
            override fun onResponse(call: Call<ImageResponse>?, response: Response<ImageResponse>?) {
                Log.d("request","success")
                data2.value=response?.body()
            }
        }))
        return data2
    }
}