package cagin.com.imgurproject.data.di

import cagin.com.imgurproject.BuildConfig
import cagin.com.imgurproject.utils.Constants
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton
import java.util.logging.Level


@Module
class RetrofitModule {

    @Provides
    @Singleton
    fun provideRetrofit (okHttpClient: OkHttpClient, @Named("apiEndpoint")apiEndpoint: String): Retrofit.Builder {

        return Retrofit.Builder()
                .baseUrl(Constants.Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
    }

    @Provides
    @Singleton
    fun provideOkHttpClient (): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC)
        val builder = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .addInterceptor(loggingInterceptor)
        return builder.build()
    }
}