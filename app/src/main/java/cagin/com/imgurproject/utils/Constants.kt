package cagin.com.imgurproject.utils

class Constants {
    object Api {
        val BASE_URL = "https://api.imgur.com/3/gallery/"
        val IMAGES = "0.json"
    }
}