package cagin.com.imgurproject.base.components

import android.app.Application
import cagin.com.imgurproject.ImgurProjectApplication
import cagin.com.imgurproject.base.builder.ActivityBuilder
import cagin.com.imgurproject.base.module.AppModule
import cagin.com.imgurproject.data.di.RetrofitModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Named
import javax.inject.Singleton

@Singleton
@Component(modules = [
    RetrofitModule::class,
    AndroidInjectionModule::class,
    AppModule::class,
    ActivityBuilder::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        abstract fun application(application: Application): Builder

        @BindsInstance
        abstract fun endPoint(@Named("apiEndpoint") apiEndpoint: String): Builder

        fun build() : AppComponent
    }
    fun inject(app: ImgurProjectApplication)
}