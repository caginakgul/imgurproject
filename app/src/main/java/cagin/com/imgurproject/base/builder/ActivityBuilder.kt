package cagin.com.imgurproject.base.builder

import cagin.com.imgurproject.feature.about.AboutActivity
import cagin.com.imgurproject.feature.about.AboutActivityModule
import cagin.com.imgurproject.feature.detail.DetailActivity
import cagin.com.imgurproject.feature.detail.DetailActivityModule
import cagin.com.imgurproject.feature.main.MainActivity
import cagin.com.imgurproject.feature.main.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    internal abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [AboutActivityModule::class])
    internal abstract fun contributeAboutActivity(): AboutActivity

    @ContributesAndroidInjector(modules = [DetailActivityModule::class])
    internal abstract fun contributeDetailActivity(): DetailActivity

}