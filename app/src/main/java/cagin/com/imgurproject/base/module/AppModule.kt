package cagin.com.imgurproject.base.module

import cagin.com.imgurproject.ImgurProjectApplication
import dagger.Module

@Module
class AppModule(val app: ImgurProjectApplication) {
//May be used further
}