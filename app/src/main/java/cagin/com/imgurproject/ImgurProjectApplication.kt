package cagin.com.imgurproject

import android.app.Activity
import android.app.Application
import cagin.com.imgurproject.base.components.DaggerAppComponent
import cagin.com.imgurproject.utils.Constants
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class ImgurProjectApplication : Application(), HasActivityInjector {
    @Inject
    lateinit var activityDispatchingAndroidInjector : DispatchingAndroidInjector<Activity>

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector
    }

    override fun onCreate() {
        super.onCreate()
        DaggerAppComponent
                .builder()
                .application(this)
                .endPoint(Constants.Api.BASE_URL)
                .build().inject(this)
    }
}