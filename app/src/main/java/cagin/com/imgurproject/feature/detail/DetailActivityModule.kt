package cagin.com.imgurproject.feature.detail

import android.arch.lifecycle.ViewModelProvider
import cagin.com.imgurproject.base.ProjectViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class DetailActivityModule {
    @Provides
    fun provideDetailViewModel(detailViewModel: DetailViewModel) : ViewModelProvider.Factory{
        return ProjectViewModelFactory(detailViewModel)
    }
}