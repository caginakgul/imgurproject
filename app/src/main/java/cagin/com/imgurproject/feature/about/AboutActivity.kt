package cagin.com.imgurproject.feature.about

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import cagin.com.imgurproject.R
import cagin.com.imgurproject.databinding.ActivityAboutBinding
import cagin.com.imgurproject.databinding.ActivityMainBinding
import cagin.com.imgurproject.feature.main.MainViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

class AboutActivity : AppCompatActivity() {
    private lateinit var binding : ActivityAboutBinding
    private lateinit var viewModel : AboutViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        initBinding()
    }

    private fun initBinding(){
        binding = DataBindingUtil.setContentView(this, R.layout.activity_about)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AboutViewModel::class.java)
    }

}