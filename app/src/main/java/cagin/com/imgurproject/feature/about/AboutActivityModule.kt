package cagin.com.imgurproject.feature.about

import android.arch.lifecycle.ViewModelProvider
import cagin.com.imgurproject.base.ProjectViewModelFactory
import cagin.com.imgurproject.feature.main.MainViewModel
import dagger.Module
import dagger.Provides

@Module
class AboutActivityModule {
    @Provides
    fun provideAboutViewModel(aboutViewModel: AboutViewModel) : ViewModelProvider.Factory{
        return ProjectViewModelFactory(aboutViewModel)
    }
}