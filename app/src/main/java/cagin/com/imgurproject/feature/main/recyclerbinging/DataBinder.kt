package cagin.com.imgurproject.feature.main.recyclerbinging

import android.databinding.BindingAdapter
import android.graphics.Color
import android.widget.ImageView
import cagin.com.imgurproject.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

@BindingAdapter("android:profileImage")
fun setImageUrl(imageView: ImageView, url: String?){
    Glide.with(imageView.context)
            .load(url)
            .apply(RequestOptions()
            .placeholder(R.drawable.placeholder)
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE))
            .into(imageView)
}