package cagin.com.imgurproject.feature.main

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import cagin.com.imgurproject.data.repository.ImageListRepository
import cagin.com.imgurproject.data.retrofit.model.ImageResponse
import javax.inject.Inject

class MainViewModel @Inject constructor(application: Application) : AndroidViewModel(application) {
    private val imageResponseObservable: MutableLiveData<ImageResponse>

    @Inject lateinit var imageListRepository:ImageListRepository

    init {
        imageResponseObservable = MutableLiveData()
    }
    fun sendImageListRequest(): LiveData<ImageResponse> {
        return imageListRepository.sendImageListRequest()
    }
}