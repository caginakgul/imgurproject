package cagin.com.imgurproject.feature.main.recyclerbinging

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import cagin.com.imgurproject.data.retrofit.model.ImageModel
import cagin.com.imgurproject.databinding.ItemImageBinding

class ImageListRecyclerAdapter(private val items: List<ImageModel>?,
                               val clickListener: (ImageModel) -> Unit) : RecyclerView.Adapter<ImageListRecyclerAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemImageBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items!!.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items!![position], clickListener)

    inner class ViewHolder(val binding: ItemImageBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ImageModel,clickListener: (ImageModel) -> Unit) {
            binding.picItem = item
            binding.executePendingBindings()
            binding.root.setOnClickListener {
                clickListener(item)
            }
        }
    }
}