package cagin.com.imgurproject.feature.main

import android.arch.lifecycle.ViewModelProvider
import cagin.com.imgurproject.base.ProjectViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class MainActivityModule {
    @Provides
    fun provideMainViewModel(mainViewModel: MainViewModel) : ViewModelProvider.Factory{
        return ProjectViewModelFactory(mainViewModel)
    }
}