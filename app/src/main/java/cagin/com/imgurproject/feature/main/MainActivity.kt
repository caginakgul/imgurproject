package cagin.com.imgurproject.feature.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import cagin.com.imgurproject.R
import cagin.com.imgurproject.data.retrofit.model.ImageModel
import cagin.com.imgurproject.data.retrofit.model.ImageResponse
import cagin.com.imgurproject.databinding.ActivityMainBinding
import cagin.com.imgurproject.feature.about.AboutActivity
import cagin.com.imgurproject.feature.detail.DetailActivity
import cagin.com.imgurproject.feature.main.recyclerbinging.ImageListRecyclerAdapter
import dagger.android.AndroidInjection
import javax.inject.Inject
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.widget.GridLayoutManager

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    private lateinit var viewModel : MainViewModel
    private val DATA_LOG: String="DATA_LOG"
    private val PARCELABLE_TAG: String="PARCELABLE_TAG"
    private val BUNDLE_TAG: String="BUNDLE_TAG"
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        initBinding()
        sendRequest()
    }

    private fun initBinding(){
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel::class.java)
        binding.imageViewAbout.setOnClickListener { startAboutActivity() }
    }

    private fun sendRequest(){
        viewModel.sendImageListRequest().observe(this, object : Observer<ImageResponse> {
            override fun onChanged(networkResponse: ImageResponse?) {
                Log.d(DATA_LOG,"retrieved.")
                if(networkResponse!=null){
                    setRecycler(networkResponse)
                    binding.progressBar.visibility= View.GONE
                }
            }
        })
    }

    private fun setRecycler(networkResponse: ImageResponse){
        binding.recyclerImageList.layoutManager  = GridLayoutManager(this, 2)
        binding.recyclerImageList.adapter = ImageListRecyclerAdapter(networkResponse?.data,
                {imageModel : ImageModel -> imageClicked(imageModel)})
    }
    private fun startAboutActivity(){
        val intent = Intent(this, AboutActivity::class.java)
        startActivity(intent)
    }

    private fun imageClicked(imageModel: ImageModel){
        val detailIntent = Intent(this, DetailActivity::class.java)
        var bundle = Bundle()
        bundle.putParcelable(PARCELABLE_TAG,imageModel)
        detailIntent.putExtra(BUNDLE_TAG,bundle)
        startActivity(detailIntent)
    }
}
