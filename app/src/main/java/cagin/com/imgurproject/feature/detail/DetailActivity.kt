package cagin.com.imgurproject.feature.detail

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import cagin.com.imgurproject.R
import cagin.com.imgurproject.data.retrofit.model.ImageModel
import cagin.com.imgurproject.databinding.ActivityDetailBinding
import dagger.android.AndroidInjection
import javax.inject.Inject

class DetailActivity : AppCompatActivity(){
    private lateinit var binding : ActivityDetailBinding
    private lateinit var viewModel : DetailViewModel
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        initBinding()
    }

    private fun initBinding(){
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailViewModel::class.java)
        val bundle = intent.getBundleExtra("BUNDLE_TAG")
        var imageModel  = bundle.getParcelable<ImageModel>("PARCELABLE_TAG") as ImageModel
        binding.model=imageModel
    }

}